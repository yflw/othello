package com.gitlab.yflw;

import org.junit.Test;

import java.util.Arrays;
import static com.gitlab.yflw.OthelloConsts.*;
import static org.junit.Assert.*;

/**
 * Created by lidavid on 28/01/2018.
 */
public class OthelloTest {

    @Test
    public void BoardInitTest(){
        Board board = Board.getInstance();
        char[][] othelloData = board.getOthelloData();

        assertEquals(PlayerId[0], othelloData[3][4]);
        assertEquals(PlayerId[0], othelloData[4][3]);

        assertEquals(PlayerId[1], othelloData[3][3]);
        assertEquals(PlayerId[1], othelloData[4][4]);
    }

    @Test
    public void OthelloConsoleInputValidateTest(){
        OthelloConsoleInput input = new OthelloConsoleInput();
        char player = PlayerId[0];

        assertFalse(input.validate("0a", player));
        assertFalse(input.validate("0i", player));
        assertFalse(input.validate("1i", player));
        assertFalse(input.validate("9a", player));
        assertFalse(input.validate("9i", player));

        assertFalse(input.validate("1a", player));
        assertFalse(input.validate("1h", player));
        assertFalse(input.validate("1A", player));
        assertFalse(input.validate("1H", player));
        assertFalse(input.validate("8a", player));
        assertFalse(input.validate("8H", player));

        //   3d
        //4c O  X
        //   X  O  5f
        //      6e
        assertTrue(input.validate("4c", player));
        assertTrue(input.validate("5f", player));
        assertTrue(input.validate("3d", player));
        assertTrue(input.validate("6e", player));
    }

    @Test
    public void OthelloConsoleInputGetPositionTest(){
        OthelloConsoleInput input =  new OthelloConsoleInput();
        assertEquals(input.getPosition("1a"), new Position(0,0));
        assertEquals(input.getPosition("1A"), new Position(0,0));
        assertEquals(input.getPosition("a1"), new Position(0,0));
        assertEquals(input.getPosition("A1"), new Position(0,0));

        assertEquals(input.getPosition("8a"), new Position(7,0));
        assertEquals(input.getPosition("8A"), new Position(7,0));
        assertEquals(input.getPosition("a8"), new Position(7,0));
        assertEquals(input.getPosition("A8"), new Position(7,0));

        assertEquals(input.getPosition("8H"), new Position(7,7));
        assertEquals(input.getPosition("8h"), new Position(7,7));
        assertEquals(input.getPosition("h8"), new Position(7,7));
        assertEquals(input.getPosition("H8"), new Position(7,7));
    }

    @Test
    public void OthelloPositionGetPositionTest(){
        Position position = new Position(0,0);
        assertNull(position.getPosition(Position.PositionDirection.N));
        assertNull(position.getPosition(Position.PositionDirection.NE));
        assertNull(position.getPosition(Position.PositionDirection.NW));

        position = new Position(7,7);
        assertNull(position.getPosition(Position.PositionDirection.S));
        assertNull(position.getPosition(Position.PositionDirection.SE));
        assertNull(position.getPosition(Position.PositionDirection.SW));

        position = new Position(0,7);
        assertNull(position.getPosition(Position.PositionDirection.N));
        assertNull(position.getPosition(Position.PositionDirection.NE));
        assertNull(position.getPosition(Position.PositionDirection.E));

        position = new Position(7,0);
        assertNull(position.getPosition(Position.PositionDirection.S));
        assertNull(position.getPosition(Position.PositionDirection.SW));
        assertNull(position.getPosition(Position.PositionDirection.W));

        position = new Position(3,3);
        assertEquals(position.getPosition(Position.PositionDirection.N), new Position(2,3));
        assertEquals(position.getPosition(Position.PositionDirection.E), new Position(3,4));
        assertEquals(position.getPosition(Position.PositionDirection.W), new Position(3,2));
        assertEquals(position.getPosition(Position.PositionDirection.S), new Position(4,3));
        assertEquals(position.getPosition(Position.PositionDirection.SE), new Position(4,4));
        assertEquals(position.getPosition(Position.PositionDirection.SW), new Position(4,2));
        assertEquals(position.getPosition(Position.PositionDirection.NE), new Position(2,4));
        assertEquals(position.getPosition(Position.PositionDirection.NW), new Position(2,2));
    }
}