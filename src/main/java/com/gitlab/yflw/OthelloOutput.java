package com.gitlab.yflw;

/**
 * Created by lidavid on 28/01/2018.
 */
public interface OthelloOutput {
    void show(Board board);
}
