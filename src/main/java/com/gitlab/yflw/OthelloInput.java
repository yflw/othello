package com.gitlab.yflw;

import java.io.IOException;

import static com.gitlab.yflw.OthelloConsts.COL_STARTAT;
import static com.gitlab.yflw.OthelloConsts.EmptyId;
import static com.gitlab.yflw.OthelloConsts.ROW_STARTAT;

/**
 * Created by lidavid on 28/01/2018.
 */
public abstract class OthelloInput {

    public static boolean validatePosition(Position position, Position.PositionDirection direction, char name) {
        Board board = Board.getInstance();

        Position detectPos = position.getPosition(direction);

        // second cell is border
        if(detectPos == null ) return  false;

        // second cell is empty
        if(board.getValue(detectPos) == EmptyId) return false;

        // second cell is same as player id
        if(board.getValue(detectPos) == name) return false;

        // second cell is for another player, it's valid, continue the detection for player
        while(detectPos != null
                && board.getValue(detectPos) != EmptyId
                && board.getValue(detectPos) != name){

            detectPos = detectPos.getPosition(direction);

            // reach the border, or empty cell
            if(detectPos == null || board.getValue(detectPos) == EmptyId)
                return false;

            // found item for player, reach end of the search
            if(board.getValue(detectPos) == name)
                return true;
        }

        return  false;
    }

    public boolean validate(String input, char player){
        if(input == null) return false;
        if(input.length() != 2 ) return false;
        if(!input.matches("([a-hA-H][1-8])|([1-8][a-hA-H])"))return false;

        Position position = getPosition(input.toUpperCase());
        Board board = Board.getInstance();

        // already bean taken
        if(board.getValue(position) != EmptyId) return false;

        //detect direction N
        Position.PositionDirection[] allDirections = Position.PositionDirection.values();
        for (int i = 0; i < allDirections.length; i++) {
            if(validatePosition(position, allDirections[i], player) == true)
                return true;
        }

        return false;
    }

    /**
     * assume the string is valid string.
     * @param input
     * @return
     */
    public Position getPosition(String input){
        String upperCaseInput = input.toUpperCase();
        if(upperCaseInput.charAt(0) <= '8' && upperCaseInput.charAt(0) >= ROW_STARTAT){
            return new Position(upperCaseInput.charAt(0) - ROW_STARTAT,upperCaseInput.charAt(1) - COL_STARTAT);
        }
        else if(upperCaseInput.charAt(0) <= 'H' && upperCaseInput.charAt(0) >= COL_STARTAT){
            return new Position(upperCaseInput.charAt(1) - ROW_STARTAT, upperCaseInput.charAt(0) - COL_STARTAT);
        }
        return null;
    }
    abstract Position readInput(Player player) throws IOException;
}
