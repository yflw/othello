package com.gitlab.yflw;

/**
 * Created by lidavid on 28/01/2018.
 */
public class OthelloConsoleOutput implements OthelloOutput {
    @Override
    public void show(Board board) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < board.getOthelloData().length; i++) {
            //set row id
            sb.append(i+1).append(" ");
            for (int j = 0; j < board.getOthelloData()[i].length; j++) {
                sb.append(board.getOthelloData()[i][j]);
            }

            // new line of row
            sb.append("\n");
        }

        // set col id
        sb.append("  abcdefgh\n");

        System.out.println(sb.toString());
    }
}
