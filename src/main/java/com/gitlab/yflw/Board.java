package com.gitlab.yflw;

import com.google.common.collect.Lists;
import sun.security.action.PutAllAction;

import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.gitlab.yflw.OthelloConsts.BOARD_SIZE;
import static com.gitlab.yflw.OthelloConsts.EmptyId;
import static com.gitlab.yflw.OthelloConsts.PlayerId;

/**
 * Created by lidavid on 28/01/2018.
 */
public class Board {
    char othelloData[][] = new char[BOARD_SIZE][BOARD_SIZE];

    private static Board instance;

    public int getBOARD_SIZE() {
        return BOARD_SIZE;
    }

    public char[][] getOthelloData() {
        return othelloData;
    }

    public Board initBoard() {
        for (char[] othelloDatum : instance.getOthelloData()) {
            Arrays.fill(othelloDatum, EmptyId);
        }

        this.setValue(3, 3, PlayerId[1]);
        this.setValue(3, 4, PlayerId[0]);
        this.setValue(4, 3, PlayerId[0]);
        this.setValue(4, 4, PlayerId[1]);

        return this;
    }

    public static Board getInstance() {
        if (instance == null) {
            instance = new Board();
            instance.initBoard();
        }

        return instance;
    }

    private Board() {

    }

    public void setValue(int row, int col, char value) {
        othelloData[row][col] = value;
    }

    public void flipCell(Position position){
        if(getValue(position) == EmptyId)
            return;

        if(getValue(position) == PlayerId[0])
            setValue(position, PlayerId[1]);
        else
            setValue(position, PlayerId[0]);
    }

    void updateCells(Position position, char value){
        Position.PositionDirection[] allDirections = Position.PositionDirection.values();
        for (int i = 0; i < allDirections.length; i++) {
            Position.PositionDirection direction = allDirections[i];

            if(OthelloInput.validatePosition(position,direction , value) == true){
                Position posDetect = position.getPosition(direction);

                while (getValue(posDetect) != value) {
                    flipCell(posDetect);
                    posDetect = posDetect.getPosition(direction);
                }
            }

        }
    }

    public void setValue(Position position, char value) {
        setValue(position.getRow(), position.getCol(), value);
    }

    public char getValue(int row, int col) {
        return othelloData[row][col];
    }

    public char getValue(Position position) {
        return getValue(position.getRow(), position.getCol());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < getOthelloData().length; i++) {
            for (int j = 0; j < getOthelloData()[i].length; j++) {
                sb.append(getOthelloData()[i][j]);
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    public boolean isFull() {
        for (int i = 0; i < getOthelloData().length; i++) {
            for (int j = 0; j < getOthelloData()[i].length; j++) {
                if (getOthelloData()[i][j] == EmptyId)
                    return false;
            }
        }

        return true;
    }

    public void calcResult(Player player1, Player player2) {
        int result1 = 0;
        int result2 = 0;
        for (int i = 0; i < getOthelloData().length; i++) {
            for (int j = 0; j < getOthelloData()[i].length; j++) {
                if (getOthelloData()[i][j] == PlayerId[0])
                    result1++;
                else if (getOthelloData()[i][j] == PlayerId[1])
                    result2++;
            }
        }

        System.out.println(String.format("Player '%s' wins ( %d vs %d )",
                (result1 > result2 ? player1.getName() : player2.getName()),
                result1, result2));
    }

}
