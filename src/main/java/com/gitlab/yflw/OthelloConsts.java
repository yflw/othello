package com.gitlab.yflw;

/**
 * Created by lidavid on 29/01/2018.
 */
public interface OthelloConsts {
    final int BOARD_SIZE = 8;

    final char ROW_STARTAT = '1';
    final char COL_STARTAT = 'A';

    final char[] PlayerId = new char[] {'X','O'};
    final char EmptyId = '-';
}
