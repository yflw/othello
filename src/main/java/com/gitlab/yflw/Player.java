package com.gitlab.yflw;

import com.google.common.collect.Lists;

import java.io.IOException;
import java.util.List;

/**
 * Created by lidavid on 28/01/2018.
 */
public class Player {
    enum PlayerType{
        Computer,
        Human
    }

    char name;
    PlayerType type;

    OthelloInput inputer = null;
    OthelloOutput outputer = new OthelloConsoleOutput();

    public Player() {
    }

    public OthelloInput getInputer() {
        return inputer;
    }

    public void setInputer(OthelloInput inputer) {
        this.inputer = inputer;
    }

    public OthelloOutput getOutputer() {
        return outputer;
    }

    public void setOutputer(OthelloOutput outputer) {
        this.outputer = outputer;
    }

    public Player(char name, PlayerType type) {
        this.name = name;
        this.type = type;
    }

    public PlayerType getType() {
        return type;
    }

    public void setType(PlayerType type) {
        this.type = type;
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public List<Position> getPlayerAvailablePositions(){

        return Lists.newArrayList();
    }
    /**
     *
     * @return false means no way to move
     */
    public boolean move() throws IOException {
        // show the board first
        Board board = Board.getInstance();
        getOutputer().show(board);

        System.out.println("Player " + getName() + " moves: ");

        Position position = getInputer().readInput(this);
        if(position == null)
            return  false;

        board.setValue(position, getName());
        board.updateCells(position, getName());
        getOutputer().show(board);

        return true;
    }
}
