package com.gitlab.yflw;

import static com.gitlab.yflw.OthelloConsts.PlayerId;

/**
 * Created by lidavid on 28/01/2018.
 */
public class AiPlayer extends Player {
    public AiPlayer() {
        this(PlayerId[1]);
    }
    public AiPlayer(char name){
        this.name = name;
        type = PlayerType.Computer;

        setInputer(new OthelloAiInput());
    }
}
