package com.gitlab.yflw;


import com.google.common.collect.Lists;

import java.io.IOException;
import java.util.List;
import static com.gitlab.yflw.OthelloConsts.*;

public class Othello{
    /**
     * 2 players:
     *   first: identify as 'X', will go first.
     *   second: identify as 'O', will go next.
     */
    static List<Player> players = Lists.newArrayList();

    public static void main(String[] args) throws IOException {
        String othello_mode = System.getProperty("OTHELLO_MODE");
        if(othello_mode == null)
            othello_mode = "HUMAN_HUMAN";

        switch (othello_mode){
            case "HUMAN_HUMAN":
                players.add(new HumanPlayer(PlayerId[0]));
                players.add(new HumanPlayer(PlayerId[1]));
                break;
            case "HUMAN_AI":
                players.add(new HumanPlayer(PlayerId[0]));
                players.add(new AiPlayer(PlayerId[1]));
                break;
            case "AI_AI":
                players.add(new AiPlayer(PlayerId[0]));
                players.add(new AiPlayer(PlayerId[1]));
                break;
            case "AI_HUMAN":
                players.add(new AiPlayer(PlayerId[0]));
                players.add(new HumanPlayer(PlayerId[1]));
                break;
            default:
                System.out.println("unknown OTHELLO_MODE " + othello_mode);
                System.exit(-1);
        }

        // play
        while(true){
            boolean player1Status = players.get(0).move();
            boolean player2Status = players.get(1).move();

            if(player1Status == false && player2Status == false) {
                System.out.println("No further moves available");
                break;
            }

            if(Board.getInstance().isFull()) {
                System.out.println("Board is full");
                break;
            }
        }

        // calculate result
        Board.getInstance().calcResult(players.get(0), players.get(1));

        System.out.println("End");
    }
}