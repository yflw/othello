package com.gitlab.yflw;

import javafx.geometry.Pos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.gitlab.yflw.OthelloConsts.*;

/**
 * Created by lidavid on 28/01/2018.
 */
public class OthelloConsoleInput extends OthelloInput {
    @Override
    public Position readInput(Player player) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String inputStr = br.readLine();

        if(validate(inputStr, player.getName()) == false){
            System.out.println("Invalid move. Please try again.");
            return readInput(player);
        }

        return getPosition(inputStr);
    }
}
