package com.gitlab.yflw;

import com.google.common.collect.Lists;
import javafx.geometry.Pos;

import java.util.Arrays;
import java.util.List;

import static com.gitlab.yflw.OthelloConsts.BOARD_SIZE;

/**
 * Created by lidavid on 28/01/2018.
 */
public class Position {
    enum PositionDirection{
        N,
        NE,
        E,
        SE,
        S,
        SW,
        W,
        NW
    }
    int row;
    int col;

    public Position(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public Position() {
        row = 0;
        col = 0;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public Position getPosition(PositionDirection direction) {
        switch (direction) {
            case N:
                return getRow() == 0 ? null : new Position(getRow() - 1, getCol());
            case NE:
                if (getRow() == 0 || getCol() == BOARD_SIZE - 1) return null;
                return new Position(getRow() - 1, getCol() + 1);
            case E:
                return getCol() == BOARD_SIZE - 1 ? null : new Position(getRow(), getCol() + 1);
            case SE:
                if (getRow() == BOARD_SIZE - 1 || getCol() == BOARD_SIZE - 1) return null;
                return new Position(getRow() + 1, getCol() + 1);
            case S:
                return getRow() == BOARD_SIZE - 1 ? null : new Position(getRow() + 1, getCol());
            case SW:
                if (getRow() == BOARD_SIZE-1 || getCol() == 0) return null;
                return new Position(getRow() +1, getCol() - 1);
            case W:
                return getCol() == 0 ? null : new Position(getRow(), getCol() - 1);
            case NW:
                if (getRow() == 0 || getCol() == 0) return null;
                return new Position(getRow() -1, getCol() - 1);
        }
        return null;
    }

    @Override
    public String toString() {
        return "Position{" +
                "row=" + row +
                ", col=" + col +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;

        Position position = (Position) o;

        if (row != position.row) return false;
        return col == position.col;
    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + col;
        return result;
    }
}
