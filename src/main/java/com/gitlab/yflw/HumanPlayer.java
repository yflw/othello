package com.gitlab.yflw;

import static com.gitlab.yflw.OthelloConsts.PlayerId;

/**
 * Created by lidavid on 28/01/2018.
 */
public class HumanPlayer extends Player {
    public HumanPlayer() {
        this(PlayerId[0]);
    }

    public HumanPlayer(char name){
        this.name = name;
        type = PlayerType.Human;

        setInputer(new OthelloConsoleInput());
    }
}
